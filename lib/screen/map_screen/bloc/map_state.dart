import 'package:flutter_base/utils/base_equatable.dart';

class MapState extends BaseEquatable {}

class MapInitialState extends MapState {}

class MapLoadingState extends MapState {}

class MapLoadedState extends MapState {}
