import 'package:flutter_base/utils/base_equatable.dart';

class MapEvent extends BaseEquatable {}

class MapInitialEvent extends MapEvent {}
