import 'package:flutter_base/utils/base_equatable.dart';

class HomeTabEvent extends BaseEquatable {}

class HomeTabInitialEvent extends HomeTabEvent {}
