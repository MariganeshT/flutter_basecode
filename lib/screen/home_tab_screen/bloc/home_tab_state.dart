import 'package:flutter_base/utils/base_equatable.dart';

class HomeTabState extends BaseEquatable {}

class HomeTabInitialState extends HomeTabState {}

class HomeTabLoadingState extends HomeTabState {}
