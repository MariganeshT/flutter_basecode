import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/authentication/authentication_bloc.dart';
import 'package:flutter_base/screen/home_screen/bloc/home_bloc.dart';
import 'package:flutter_base/screen/home_screen/bloc/home_event.dart';
import 'package:flutter_base/screen/home_screen/home_screen.dart';
import 'package:flutter_base/screen/home_tab_screen/bloc/home_tab_bloc.dart';
import 'package:flutter_base/screen/map_screen/bloc/map_bloc.dart';
import 'package:flutter_base/screen/map_screen/bloc/map_event.dart';
import 'package:flutter_base/screen/map_screen/map_screen.dart';
import 'package:flutter_base/screen/select_bank_screen/bloc/select_bank_bloc.dart';
import 'package:flutter_base/screen/select_bank_screen/bloc/select_bank_event.dart';
import 'package:flutter_base/screen/select_bank_screen/select_bank_screen.dart';
import 'package:flutter_base/screen/webview_screen/bloc/webview_bloc.dart';
import 'package:flutter_base/screen/webview_screen/bloc/webview_event.dart';
import 'package:flutter_base/screen/webview_screen/webview_screen.dart';
import 'package:flutter_base/utils/font.dart';
import 'package:flutter_base/utils/string_resource.dart';
import 'package:flutter_base/widgets/custom_text.dart';
import 'package:flutter_base/widgets/gradientButton.dart';
import 'package:flutter_base/widgets/hyperlink_widget.dart';
import 'package:flutter_base/widgets/primary_button.dart';
import 'package:flutter_base/widgets/custom_textfield.dart';
import 'package:flutter_base/widgets/input_label_widget.dart';
import 'package:flutter_base/widgets/secondary_button.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

// ignore: must_be_immutable
class HomeTabScreen extends StatefulWidget {
  AuthenticationBloc authenticationBloc;
  HomeTabScreen(this.authenticationBloc);
  @override
  _HomeTabScreenState createState() => _HomeTabScreenState();
}

class _HomeTabScreenState extends State<HomeTabScreen> {
  HomeTabBloc bloc;
  HomeBloc homeBloc;
  WebviewBloc webviewBloc;
  SelectBankBloc selectBankBloc;
  MapBloc mapBloc;

  @override
  void initState() {
    // TODO: implement initState
    bloc = BlocProvider.of<HomeTabBloc>(context);
    homeBloc = HomeBloc()..add(HomeInitialEvent());
    webviewBloc = WebviewBloc()..add(WebviewInitialEvent());
    selectBankBloc = SelectBankBloc()..add(SelectbankInitialEvent());
    mapBloc = MapBloc()..add(MapInitialEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
        tabBar: CupertinoTabBar(items: [
          const BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.home), label: StringResource.home),
          const BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.search), label: StringResource.webview),
          const BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.money_dollar),
              label: StringResource.bank),
          const BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.map), label: StringResource.map)
        ]),
        tabBuilder: (context, index) {
          switch (index) {
            case 0:
              return HomeScreen(homeBloc);
              break;
            case 1:
              return WebviewScreen(webviewBloc);
              break;
            case 2:
              return SelectBankScreen(selectBankBloc);
              break;
            case 3:
              return MapScreen(mapBloc);
              break;
            default:
              return Container(
                color: Colors.pink,
              );
              break;
          }
        });
  }
}
