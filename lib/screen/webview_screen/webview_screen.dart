import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/screen/webview_screen/bloc/webview_bloc.dart';
import 'package:flutter_base/utils/font.dart';
import 'package:flutter_base/utils/string_resource.dart';
import 'package:flutter_base/widgets/custom_text.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'bloc/webview_state.dart';

class WebviewScreen extends StatefulWidget {
  WebviewBloc bloc;
  WebviewScreen(this.bloc);
  @override
  _WebviewScreenState createState() => _WebviewScreenState();
}

class _WebviewScreenState extends State<WebviewScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocListener(
      cubit: widget.bloc,
      listener: (BuildContext context, WebviewState state) {},
      child: Scaffold(
        appBar: AppBar(
          title: CustomText(
            StringResource.webview,
            color: Colors.white,
            font: Font.robotoBold,
            fontSize: FontSize.twenty,
          ),
        ),
        body: WebView(
          initialUrl: 'https://flutter.dev',
        ),
      ),
    );
  }
}
