import 'package:flutter_base/utils/base_equatable.dart';

class WebViewEvent extends BaseEquatable {}

class WebviewInitialEvent extends WebViewEvent {}
