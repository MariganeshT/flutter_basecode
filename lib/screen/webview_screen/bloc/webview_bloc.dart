import 'package:bloc/bloc.dart';
import 'package:flutter_base/screen/webview_screen/bloc/webview_event.dart';
import 'package:flutter_base/screen/webview_screen/bloc/webview_state.dart';

class WebviewBloc extends Bloc<WebViewEvent, WebviewState> {
  WebviewBloc() : super(WebviewInitialState());

  @override
  Stream<WebviewState> mapEventToState(WebViewEvent event) async* {}
}
