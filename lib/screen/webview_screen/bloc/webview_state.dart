import 'package:flutter_base/utils/base_equatable.dart';

class WebviewState extends BaseEquatable {}

class WebviewInitialState extends WebviewState {}
