import 'package:flutter_base/utils/base_equatable.dart';

class HomeEvent extends BaseEquatable {}

class HomeInitialEvent extends HomeEvent {}

class HomeSubmitButtonTappedEvent extends HomeEvent {}
