import 'package:flutter_base/utils/base_equatable.dart';

class HomeState extends BaseEquatable {}

class HomeInitialState extends HomeState {
  String error;
  HomeInitialState({this.error});
}

class HomeLoadingState extends HomeState {}

class HomeRefreshState extends HomeState {}
