import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/authentication/authentication_bloc.dart';
import 'package:flutter_base/utils/color_resource.dart';
import 'package:flutter_base/utils/font.dart';
import 'package:flutter_base/utils/string_resource.dart';
import 'package:flutter_base/widgets/custom_text.dart';

class SplashScreen extends StatefulWidget {
  AuthenticationBloc authBloc;
  SplashScreen(this.authBloc);
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorResource.color0066cc,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
              child: Icon(
            CupertinoIcons.home,
            size: 50,
            color: Colors.white,
          )),
          SizedBox(
            height: 30,
          ),
          CustomText(
            StringResource.splashScreen,
            color: Colors.white,
            fontSize: FontSize.twenty,
          )
        ],
      ),
    );
  }
}
