import 'package:bloc/bloc.dart';
import 'package:flutter_base/models/bank.dart';
import 'package:flutter_base/models/branch.dart';
import 'package:flutter_base/models/language.dart';
import 'package:flutter_base/screen/select_bank_screen/bloc/select_bank_event.dart';
import 'package:flutter_base/screen/select_bank_screen/bloc/select_bank_state.dart';

class SelectBankBloc extends Bloc<SelectBankEvent, SelectBankState> {
  SelectBankBloc() : super(SelectbankInitialState());

  List<Bank> banks = [
    Bank('1', 'Indian Bank'),
    Bank('2', 'Indian Overseas Bank'),
    Bank('3', 'Punjab and Sind Bank'),
    Bank('4', 'Punjab National Bank'),
    Bank('5', 'Bank of Baroda'),
    Bank('6', 'Bank of India'),
    Bank('7', 'Canara Bank'),
    Bank('8', 'State Bank of India'),
    Bank('9', 'UCO Bank'),
    Bank('10', 'Union Bank of India')
  ];

  List<Branch> branches = [
    Branch('1', 'Arcot'),
    Branch('2', 'Chengalpattu'),
    Branch('3', 'Chennai'),
    Branch('4', 'Madurai'),
    Branch('5', 'Salem'),
    Branch('6', 'Tuticorin'),
    Branch('7', 'Tirunelveli'),
    Branch('8', 'kanyakumari'),
    Branch('9', 'Nagarcoil'),
    Branch('10', 'Coimbatore')
  ];

  List<Language> languages = [
    Language('English', 'en', 'US'),
    Language('Arabic', 'ar', 'AE'),
  ];

  Bank selectedBank;
  Branch selectedBranch;
  Language selectedlanguage;

  @override
  Stream<SelectBankState> mapEventToState(SelectBankEvent event) async* {
    if (event is DraggableSheetEvent) {
      yield DraggableSheetState();
    }
    if (event is DraggableSheetDismissEvent) {
      yield SelectBankRefreshState();
    }

    if (event is MultiSelectDraggableSheetEvent) {
      yield MultiSelectDraggableSheetState();
    }
  }
}
