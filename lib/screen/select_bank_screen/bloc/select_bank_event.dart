import 'package:flutter_base/utils/base_equatable.dart';

class SelectBankEvent extends BaseEquatable {}

class SelectbankInitialEvent extends SelectBankEvent {}

class DraggableSheetEvent extends SelectBankEvent {}

class MultiSelectDraggableSheetEvent extends SelectBankEvent {}

class DraggableSheetDismissEvent extends SelectBankEvent {}
