import 'package:flutter_base/utils/base_equatable.dart';

class SelectBankState extends BaseEquatable {}

class SelectbankInitialState extends SelectBankState {}

class DraggableSheetState extends SelectBankState {}

class MultiSelectDraggableSheetState extends SelectBankState {}

class SelectBankRefreshState extends SelectBankState {}
