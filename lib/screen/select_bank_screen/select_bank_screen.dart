import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/models/bank.dart';
import 'package:flutter_base/models/branch.dart';
import 'package:flutter_base/models/language.dart';
import 'package:flutter_base/screen/select_bank_screen/bloc/select_bank_bloc.dart';
import 'package:flutter_base/utils/font.dart';
import 'package:flutter_base/utils/string_resource.dart';
import 'package:flutter_base/widgets/custom_text.dart';
import 'package:flutter_base/widgets/multi_selection_bottom_sheet.dart';
import 'package:flutter_base/widgets/primary_button.dart';
import 'package:flutter_base/widgets/single_selection_bottom_sheet.dart';
import 'package:flutter_base/widgets/dropdown.dart';
import 'package:flutter_base/widgets/gradientButton.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../main.dart';
import 'bloc/select_bank_event.dart';
import 'bloc/select_bank_state.dart';

class SelectBankScreen extends StatefulWidget {
  SelectBankBloc bloc;

  SelectBankScreen(this.bloc);
  @override
  _SelectBankScreenState createState() => _SelectBankScreenState();
}

class _SelectBankScreenState extends State<SelectBankScreen> {
  @override
  Widget build(BuildContext context) {
    double height = (346 / MediaQuery.of(context).size.height);
    return BlocListener(
        cubit: widget.bloc,
        listener: (BuildContext context, SelectBankState state) {},
        child: BlocBuilder(
          cubit: widget.bloc,
          builder: (BuildContext context, SelectBankState state) {
            return Scaffold(
              appBar: AppBar(
                  title: CustomText(
                StringResource.bank,
                color: Colors.white,
                font: Font.robotoBold,
                fontSize: FontSize.twenty,
              )),
              body: Stack(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            margin: const EdgeInsets.only(
                                left: 16, right: 16, top: 20, bottom: 10),
                            child: CustomText(
                              StringResource.bank,
                              font: Font.robotoBold,
                              fontWeight: FontWeights.bold,
                              fontSize: FontSize.eighteen,
                            )),
                        Container(
                          margin: const EdgeInsets.symmetric(horizontal: 16),
                          width: double.infinity,
                          child: DropDown(
                              widget.bloc.banks, widget.bloc.selectedBank,
                              (Bank selected) {
                            widget.bloc.selectedBank = selected;
                            setState(() {});
                          }),
                        ),
                        Container(
                            margin: const EdgeInsets.only(
                                left: 16, right: 16, top: 20, bottom: 10),
                            child: CustomText(
                              StringResource.branch,
                              font: Font.robotoBold,
                              fontWeight: FontWeights.bold,
                              fontSize: FontSize.eighteen,
                            )),
                        Container(
                          margin: const EdgeInsets.symmetric(horizontal: 16),
                          width: double.infinity,
                          child: DropDown(
                              widget.bloc.branches, widget.bloc.selectedBranch,
                              (Branch selected) {
                            widget.bloc.selectedBranch = selected;
                            setState(() {});
                          }),
                        ),
                        Container(
                            margin: const EdgeInsets.only(
                                left: 16, right: 16, top: 20, bottom: 10),
                            child: CustomText(
                              StringResource.language,
                              font: Font.robotoBold,
                              fontWeight: FontWeights.bold,
                              fontSize: FontSize.eighteen,
                            )),
                        // Container(
                        //   margin: const EdgeInsets.symmetric(horizontal: 16),
                        //   width: double.infinity,
                        //   child: DropDown(widget.bloc.languageList,
                        //       widget.bloc.selectedlanguage,
                        //       (Language selected) {
                        //     widget.bloc.selectedlanguage = selected;
                        //     setState(() {});
                        //   }),
                        // ),
                        Container(
                          margin: const EdgeInsets.all(20),
                          child: PrimaryButton(
                              StringResource.showSingleSelectionBottomSheet,
                              onClick: () {
                            widget.bloc.add(DraggableSheetEvent());
                          }),
                        ),
                        Container(
                          margin: const EdgeInsets.all(20),
                          child: GradientButton(
                              StringResource.showMultiSelectionBottomSheet,
                              const LinearGradient(
                                colors: <Color>[Colors.pink, Colors.blue],
                              ), onClick: () {
                            widget.bloc.add(MultiSelectDraggableSheetEvent());
                          }),
                        ),
                      ],
                    ),
                  ),
                  if (state is DraggableSheetState)
                    SingleSelectionBottomSheet(widget.bloc.languages, () {
                      widget.bloc.add(DraggableSheetDismissEvent());
                    }, (Language language) async {
                      widget.bloc.languages.forEach((element) {
                        element.isSelected = false;
                      });
                      language.isSelected = true;
                      widget.bloc.selectedlanguage = language;
                      widget.bloc.add(DraggableSheetDismissEvent());
                      EasyLocalization.of(context).setLocale(
                          Locale(language.languageCode, language.locationCode));
                      // setState(() {});
                      MyApp.setLocale(context,
                          Locale(language.languageCode, language.locationCode));
                    }),
                  if (state is MultiSelectDraggableSheetState)
                    MultiSelectionBottomSheet(widget.bloc.banks, () {
                      widget.bloc.add(DraggableSheetDismissEvent());
                    }, (List<Bank> bankList) {
                      widget.bloc.add(DraggableSheetDismissEvent());
                    }),
                ],
              ),
            );
          },
        ));
  }
}
