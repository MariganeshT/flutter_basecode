import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base/screen/home_tab_screen/bloc/home_tab_bloc.dart';
import 'package:flutter_base/screen/home_tab_screen/bloc/home_tab_event.dart';
import 'package:flutter_base/screen/home_tab_screen/home_tab_screen.dart';
import 'package:flutter_base/screen/splash_screen/splash_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'authentication/authentication_bloc.dart';
import 'authentication/authentication_state.dart';

class AppRoutes {
  static const splashScreen = 'splash_screen';
  static const homeTabScreen = 'home_tab_screen';
}

// ignore: missing_return
Route<dynamic> getRoute(RouteSettings settings) {
  switch (settings.name) {
    case AppRoutes.splashScreen:
      return _buildSplashScreen();
    case AppRoutes.homeTabScreen:
      return _buildHomePage(settings);
  }
}

Route<dynamic> _buildSplashScreen() {
  return MaterialPageRoute(
    builder: (context) => addAuthBloc(context, PageBuilder.buildSplashScreen()),
  );
}

Route<dynamic> _buildHomePage(RouteSettings settings) {
  return MaterialPageRoute(builder: (context) {
    final AuthenticationBloc authBloc =
        BlocProvider.of<AuthenticationBloc>(context);
    return addAuthBloc(context, PageBuilder.buildBottomTabPage(authBloc));
  });
}

class PageBuilder {
  // Prefer (✔️) - SplashScreen Screen
  static Widget buildSplashScreen() {
    AuthenticationBloc authBloc;
    return BlocProvider(
      create: (context) {
        authBloc = BlocProvider.of<AuthenticationBloc>(context);
      },
      child: SplashScreen(authBloc),
    );
  }

  //BottomNavBar(authBloc);
  static Widget buildBottomTabPage(AuthenticationBloc authBloc) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeTabBloc>(
          create: (BuildContext context) {
            return HomeTabBloc()..add(HomeTabInitialEvent());
          },
        ),
      ],
      child: HomeTabScreen(authBloc),
    );
  }
}

Widget addAuthBloc(BuildContext context, Widget widget) {
  return BlocListener(
    cubit: BlocProvider.of<AuthenticationBloc>(context),
    listener: (context, state) {
      if (state is AuthenticationAuthenticated) {
        while (Navigator.canPop(context)) {
          Navigator.pop(context);
        }
        Navigator.pushReplacementNamed(context, AppRoutes.homeTabScreen);
        // Navigator.pushReplacementNamed(context, AppRoutes.homepatient);
        // Navigator.pushNamed(context, AppRoutes.homepatient);
      }

      
    },
    child: BlocBuilder(
      cubit: BlocProvider.of<AuthenticationBloc>(context),
      builder: (context, state) {
        return widget;
      },
    ),
  );
}
