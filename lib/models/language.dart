import 'package:flutter_base/utils/selectable.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class Language extends Selectable {
  String name;
  String languageCode;
  String locationCode;
  

  Language(this.name, this.languageCode, this.locationCode);

  @override
  // TODO: implement displayName
  String get displayName => name;
}
