import 'package:flutter_base/utils/selectable.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class Bank extends Selectable {
  String id;
  String name;
  Bank(this.id, this.name);

  @override
  // TODO: implement displayName
  String get displayName => name;
}
