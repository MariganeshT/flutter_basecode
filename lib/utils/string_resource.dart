// class StringResource {
//   static const String splashScreen = 'Splash Screen';
//   static const String userName = 'User Name';
//   static const String enterYourName = 'Enter Your Name';
//   static const String email = 'Email';
//   static const String mobileNumber = 'Mobile Number';
//   static const String enterMobileNumber = 'Enter Mobile Number';
//   static const String enterYourEmail = 'Enter Your Email';
//   static const String submit = 'Submit';
//   static const String cancel = 'Cancel';
//   static const String done = 'Done';
//   static const String termsCondition = 'Terms and conditions';
//   static const String login = 'Login';
//   static const String home = 'Home';
//   static const String webview = 'Webview';
//   static const String bank = 'Bank';
//   static const String select = 'Select';
//   static const String selectBank = 'Select Bank';
//   static const String selectBranch = 'Select Branch';
//   static const String branch = 'Branch';
//   static const String language = 'Language';
//   static const String showCustomAlertDialog = 'Show Custom Alert Dialog';
//   static const String showAlertDialog = 'Show Alert Dialog';
//   static const String okay = 'Okay';
//   static const String customDialog = 'Custom Dialog';
//   static const String customDialogDiscription =
//       'Hii all this is a custom dialog in flutter and  you will be use in your flutter applications';
//   static const String askLocationPermission = 'Ask Location Permission';
//   static const String showSingleSelectionBottomSheet =
//       'Show Single Selection Bottom Sheet';
//   static const String showMultiSelectionBottomSheet =
//       'Show Multi Selection Bottom Sheet';
//   static const String search = 'Search...';
// }

class StringResource {
  static const String splashScreen = 'splashScreen';
  static const String userName = 'userName';
  static const String enterYourName = 'enterYourName';
  static const String email = 'email';
  static const String mobileNumber = 'mobileNumber';
  static const String enterMobileNumber = 'enterMobileNumber';
  static const String enterYourEmail = 'enterYourEmail';
  static const String submit = 'submit';
  static const String cancel = 'cancel';
  static const String done = 'done';
  static const String termsCondition = 'termsCondition';
  static const String login = 'login';
  static const String home = 'home';
  static const String webview = 'webview';
  static const String bank = 'bank';
  static const String map = 'map';
  static const String select = 'select';
  static const String selectBank = 'selectBank';
  static const String selectBranch = 'selectBranch';
  static const String branch = 'branch';
  static const String language = 'language';
  static const String showCustomAlertDialog = 'showCustomAlertDialog';
  static const String showAlertDialog = 'showAlertDialog';
  static const String okay = 'okay';
  static const String customDialog = 'customDialog';
  static const String customDialogDiscription = 'customDialogDiscription';
  static const String askLocationPermission = 'askLocationPermission';
  static const String showSingleSelectionBottomSheet =
      'showSingleSelectionBottomSheet';
  static const String showMultiSelectionBottomSheet =
      'showMultiSelectionBottomSheet';
  static const String search = 'search';
}
