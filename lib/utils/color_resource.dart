import 'package:flutter/cupertino.dart';

class ColorResource {
  static const Color color0066cc = Color(0xff0066cc);
  static const Color color1c1d22 = Color(0xff1c1d22);
  static const Color color000000 = Color(0xff000000);
  static const Color color232222 = Color(0xff232222);
  static const Color colorECEBf1 = Color(0xffecebf1);
  static const Color colorD1D1D1 = Color(0xffd1d1d1);
  static const Color colorDEE0E2 = Color(0xffDEE0E2);
  static const Color colore3e3e5 = Color(0xffe3e3e5);
  static const Color colorbfbfbf = Color(0xffbfbfbf);
  static const Color colora2d6ff = Color(0xffa2d6ff);
  static const Color color414b5a = Color(0xff414b5a);
  static const Color color979797 = Color(0xff979797);
  static const Color color1C1C1C = Color(0xff1c1c1c);
  static const Color colorebeff1 = Color(0xffebeff1);
}
