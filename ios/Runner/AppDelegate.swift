import UIKit
import Flutter
import GoogleMaps


@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    
    // GMSServices.provideAPIKey("AIzaSyA9qviU5kSvtIXNfw6x5b7mkTEd3h3nP_M")
    GMSServices.provideAPIKey("AIzaSyAduzRDb5G3FTvsNZ3QF1Eqa0ypNkxhgyw")
    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
